# AWS Minecraft

## So like running minecraft in AWS

### Basic Setup Steps
So everything you had on your notebook pluuuuuuussss

#### VPC Setup
I create a new VPC, and in order to use it, you will need to create these things, in this order

* New VPC, give it a good name
* * Then pick a CIDR block, this is the big one you'll break into subnets later, so don't sweat this one too much, I used a /19 (10.10.0.0/19 specifically)
* Subnets! You're gonna make 4, I also used descriptive names here. These use small sections of the CIDR block, I used /23 blocks
* * accountname-private-az1/2
* * accountname-private-az1/2
* NAT Gateways. Again with a good name, and then they attach to _public_ subnets. So I do:
* * accountname-ngw-az1/2 attached where you'd think. Get a new elastic IP for each NAT Gateway
* Internet Gateway, give it a descriptive name, and attach it to your VPC
* Routes. You will need 3, 2 private, and 1 public, these will map to the subnets you expect
* * accountname-public-routes maps to your 2 public subnets
* * accountname-private-az1/2 map to your private subnets respectively
* Here's where the fun starts, linking all the routes together.
* * On your public route table, add a 0.0.0.0/0 pointing at the Internet Gateway you created
* * On each of your private route tables, add a 0.0.0.0/0 pointing at your NAT Gateway for that AZ.

Then, I would generate a new keypaid, throw that in Lastpass/Whatever, and spin up a free ec2 instance to test things out

Note: Might need to make a transit gateway to allow public instances to talk back to private ones, we can explore in the AM. (That or we set up some route tables, we'll look into it)

### Manual

### Cloudformation

### Terraform
